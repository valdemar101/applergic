const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const foodSchema = new Schema(
  {
    name: { type: String, required: true  },
    photo: { type: String, required: true },
    composition: { type: String },
    allergens: [{ type: mongoose.Types.ObjectId, ref: 'Allergens', required: true }],
  },
  {
    timestamps: true,
  }
);

const Food = mongoose.model('Food', foodSchema);

// export default Food;
module.exports = Food;