const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;
const bcrypt = require('bcrypt');

const User = require('../models/User');

const saltRounds = 10;

passport.use(
    'register',
    new LocalStrategy(
        {
          usernameField: 'email',
          passwordField: 'password',
          passReqToCallback: true,
        },

        async (req, email, password, done) => {
          try {
            const previousUser = await User.findOne({ email: email });
            if (previousUser) {
              const error = new Error('The user is already registered');
              return done(error);
            }
            const photo = req.body.photo ? req.body.photo : null;
            // const photo = req.file_url || "no photo";
            const hash = await bcrypt.hash(password, saltRounds);
            const contact = {};
            if (req.body.contact){
              contact.name = req.body.contact.name,
              contact.email = req.body.contact.email,
              contact.phone = req.body.contact.phone
            }
            const insuranceCompany = {};
            if (req.body.insuranceCompany){
              insuranceCompany.name = req.body.insuranceCompany.name,
              insuranceCompany.insuranceNumber = req.body.insuranceCompany.insuranceNumber,
              insuranceCompany.phone = req.body.insuranceCompany.phone
            }
            const newUser = new User({
              name: req.body.name,
              email: email,
              password: hash,
              phone: req.body.phone,
              photo: photo,
              contact: contact,
              insuranceCompany: insuranceCompany,
              favFood: req.body.favFood,
              allergies: req.body.allergies,
            });
            const savedUser = await newUser.save();
            console.log('User Registrado');
            done(null, savedUser);
          } catch (err) {
            return done(err);
          }
        },
    ),
);

passport.use(
  'login',
    new LocalStrategy(
      {
        usernameField: 'email',
        passwordField: 'password',
        passReqToCallback: true,
      },
      async (req, email, password, done) => {
        try {
          if (!req.user){
            const currentUser = await User.findOne({ email: email });

            if (!currentUser) {
              const error = new Error('The user does not exist!');
              return done(error);
            }

            const isValidPassword = await bcrypt.compare(password, currentUser.password);

            if (!isValidPassword) {
              const error = new Error('The username & password combination is incorrect!');
              return done(error);
            }
            console.log('Usuario logeado correctamente');
            done(null, currentUser);
          }
          else
          {
            console.log('Ya hay usuario logeado');
            return done(null, req.user);
          }
        } catch (err) {
          return done(err);
        }
      },
  ),
);

passport.serializeUser(async (user, done) => {
  const userId = await user._id;
  return done(null, userId);
});

passport.deserializeUser(async (userId, done) => {
  try {
    const existingUser = await User.findById(userId);
    return done(null, existingUser);
  } catch (err) {
    return done(err);
  }
});
