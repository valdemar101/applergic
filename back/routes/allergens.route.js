const express = require('express');

// eslint-disable-next-line new-cap
const router = express.Router();

const Allergens = require('../models/Allergens');

router.get('/', async (req, res, next) => {
  try {
    const allergens = await Allergens.find();
    // para ordenar alfabeticamente
    allergens.sort(function(a, b){
      if(a.name < b.name) { return -1; }
      if(a.name > b.name) { return 1; }
      return 0;
    })
    return res.status(200).json(allergens);
  } catch (err) {
    next(err);
  }
});

router.get('/:id', async (req, res, next) => {
  try{
    const allergenId = req.params.id;
    const allergen = await Allergens.findById(allergenId);
    return res.status(200).json(allergen);
  } catch(err){
    return res.status(200).send('Error. No existe ningún alérgeno con esa ID');
  }
})

router.get('/name/:name', async (req, res, next) => {
  try {
    const allergenName = req.params.name;
    const allergen = await Allergens.findOne({name: allergenName});
    if (allergen){
      return res.status(200).json(allergen);
    }
    else{
      return res.status(200).send('No existe alérgeno');
    }
  } catch (err) {
    next(err);
  }
});

module.exports = router;