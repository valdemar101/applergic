const express = require('express');
const { populate } = require('../models/Food');

// eslint-disable-next-line new-cap
const router = express.Router();

const Food = require('../models/Food');

router.get('/', async (req, res, next) => {
  try {
    const food = await Food.find()
    .populate('allergens');
    return res.status(200).json(food);
  } catch (err) {
    next(err);
  }
});

router.get('/:id', async (req, res, next) => {
  try{
    const foodId = req.params.id;
    const food = await Food.findById(foodId)
    .populate('allergens');
    return res.status(200).json(food);
  } catch(err){
    return res.status(200).send('Error. No existe ningún alimento con esa ID');
  }
})

router.get('/name/:name', async (req, res, next) => {
  try {
    const foodName = req.params.name;
    const food = await Food.findOne({name: {'$regex': `^${foodName}$`, '$options': 'i'} })
    .populate('allergens');
    if (food){
      return res.status(200).json(food);
    }
    else{
      return res.status(200).send('No existe el alimento');
    }
  } catch (err) {
    next(err);
  }
});

module.exports = router;