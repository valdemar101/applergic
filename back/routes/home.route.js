const express = require('express');

// eslint-disable-next-line new-cap
const router = express.Router();

router.get('/', (req, res, next) => {
  try {
    res.status(200).send('Bienvenidos a nuestra super APP de alérgenos');
  } catch (err) {
    next(err);
  }
});

module.exports = router;
