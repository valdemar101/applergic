require('dotenv').config();
const express = require('express');
const cors = require('cors');
require('./config/db');
require('./config/passport');

const path = require('path');
const passport = require('passport');
const session = require('express-session');
const MongoStore = require('connect-mongo');

const app = express();
const PORT = process.env.PORT || 4000;

const { DB_URL } = require('./config/db');

const homeRoute = require('./routes/home.route');
const foodRoute = require('./routes/food.route');
const allergensRoute = require('./routes/allergens.route');
const usersRoute = require('./routes/users.route');

app.use(express.json());
app.use(express.urlencoded({ extended: true }));

app.use(
  session({
    // secret: process.env.SESSION_SECRET,
    secret: "upgrade-project",
    resave: false,
    saveUninitialized: false,
    cookie: {
      maxAge: 3600000,
    },
    store: MongoStore.create({
      mongoUrl: DB_URL,
    }),
  }),
);

app.use(passport.initialize());
app.use(passport.session());

app.use(cors({ credentials: true, origin: 'http://localhost:5000'}));

app.use('/', homeRoute);
app.use('/food', foodRoute);
app.use('/allergens', allergensRoute);
app.use('/users', usersRoute);

app.use(express.static(path.join(__dirname, 'public')));

app.use('*', (req, res, next) => {
  const error = new Error('Route not found');
  error.status = 404;
  next(error); // Lanzamos la función next() con un error
});

app.use((err, req, res) => {
  return res.status(err.status || 500).render('error', {
    message: err.message || 'Unexpected error',
    status: err.status || 500,
  });
});

app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'hbs');

app.listen(PORT, () => {
  console.log(`Listening in http://localhost:${PORT}`);
});

