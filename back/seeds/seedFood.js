const mongoose = require("mongoose");

const { connection } = require("../config/db");
const Allergens = require("../models/Allergens");

const Food = require("../models/Food");

const foodArray = [
  {
    name: "Arroz con leche",
    photo: "/assets/photoFood/Arrozconleche.jpg",
    composition: "Arroz, Leche, Azúcar, Canela, Anís",
    allergens: ["Lacteos", "Sulfitos"],
  },
  {
    name: "Bonito con tomate",
    photo: "/assets/photoFood/Bonitocontomate.jpg",
    composition: "Bonito, Tomate, Cebolla, Vino blanco",
    allergens: ["Pescado", "Sulfitos"],
  },
  {
    name: "Calamares en su tinta con arroz",
    photo: "/assets/photoFood/Calamaresensutintaconarroz.jpg",
    composition: "Calamares, Cebolla, Vino blanco, Tinta de calamar, Arroz",
    allergens: ["Moluscos", "Sulfitos"],
  },
  {
    name: "Colacao",
    photo: "/assets/photoFood/Colacao.jpg",
    composition: "Cacao, harina, Malta de cebada, Nuez de cola",
    allergens: ["Cacao", "Trigo"],
  },
  {
    name: "Crema de cacahuete",
    photo: "/assets/photoFood/Cremadecacahuetes.jpg",
    composition: "Cacahuetes, Mantequilla, Aceite de sésamo",
    allergens: ["Cacahuetes", "Lacteos", "Sesamo"],
  },
  {
    name: "Crema de zanahoria y apio",
    photo: "/assets/photoFood/Cremadezanahoriayapio.jpg",
    composition:
      "Zanahoria, Apio, Puerro, Cebolla, Ajo, Oregano, Aceite, Frutos secos",
    allergens: ["Apio", "Frutos secos"],
  },
  {
    name: "Crepes",
    photo: "/assets/photoFood/Crepes.jpg",
    composition: "Leche, Harina, Huevos",
    allergens: ["Huevos", "Lacteos", "Trigo"],
  },
  {
    name: "Croquetas",
    photo: "/assets/photoFood/Croquetas.jpg",
    composition: "Leche, Harina, Mantequilla, Huevos, Pan rallado",
    allergens: ["Huevos", "Lacteos", "Trigo"],
  },
  {
    name: "Donuts",
    photo: "/assets/photoFood/Donuts.jpg",
    composition: "Azucar, Levadura, Harina",
    allergens: ["Lacteos", "Trigo"],
  },
  {
    name: "Empanadillas de atún",
    photo: "/assets/photoFood/Empanadillasdeatun.jpg",
    composition: "Harina, Mantequilla, Leche, Salsa de tomate, Atún",
    allergens: ["Lacteos", "Pescado", "Trigo"],
  },
  {
    name: "Falafel",
    photo: "/assets/photoFood/Falafel.jpg",
    composition:
      "Garbanzos, Cebolla, Cilantro, Pimienta negra, Comino, Harina de garbazos",
    allergens: ["Sulfitos"],
  },
  {
    name: "Leche",
    photo: "/assets/photoFood/Leche.jpg",
    composition: "Leche",
    allergens: ["Lacteos"],
  },
  {
    name: "Nocilla",
    photo: "/assets/photoFood/Nocilla.jpg",
    composition: "Avellanas, Cacao, Leche, Azúcar",
    allergens: ["Cacao", "Frutos secos", "Lacteos"],
  },
  {
    name: "Palomitas",
    photo: "/assets/photoFood/Palomitas.jpg",
    composition: "Maiz, Mantequilla, sal",
    allergens: ["Maiz"],
  },
  {
    name: "Pan",
    photo: "/assets/photoFood/Pan.jpg",
    composition: "Harina, Agua",
    allergens: ["Trigo", "Huevos"],
  },
  {
    name: "Pasta",
    photo: "/assets/photoFood/Pasta.jpg",
    composition: "Harina, Huevo",
    allergens: ["Huevos", "Trigo"],
  },
  {
    name: "Patatas a las finas hierbas",
    photo: "/assets/photoFood/Patatasalasfinashierbas.jpg",
    composition: "Patatas, Perejil, Tomillo, Cebollino ",
    allergens: ["Sulfitos"],
  },
  {
    name: "Paté de altramuces",
    photo: "/assets/photoFood/Patedealtramuces.jpg",
    composition: "Tomates, Altramuces, Orégano, Aceite",
    allergens: ["Altramuz"],
  },
  {
    name: "Pollo a la mostaza",
    photo: "/assets/photoFood/Polloalamostaza.jpg",
    composition: "Pollo, Mostazas, Cebolla",
    allergens: ["Mostaza"],
  },
  {
    name: "Salpicón",
    photo: "/assets/photoFood/Salpicon.jpg",
    composition: "Gambas, Surimi, Merluza, Pimientos, Cebolla, Vinagre, Aceite",
    allergens: ["Crustaceos", "Pescado", "Sulfitos"],
  },
  {
    name: "Tortilla",
    photo: "/assets/photoFood/Tortilla.jpg",
    composition: "Huevos, Patatas, Cebolla, Aceite",
    allergens: ["Huevos", "Sulfitos"],
  },
  {
    name: "Tortos con huevo y queso de Cabrales",
    photo: "/assets/photoFood/Tortoconhuevoyquesocabrales.jpg",
    composition: "Harina de maiz, Huevos, Queso de Cabrales",
    allergens: ["Huevos", "Lacteos", "Maiz"],
  },
  {
    name: "Wok de verduras con soja",
    photo: "/assets/photoFood/Wokdeverdurasconsoja.jpg",
    composition:
      "Pimiento rojo, Pimiento verde, Judias verdes, Brócoli, Zanahorias, Salsa de soja, Caldo de verduras ",
    allergens: ["Sulfitos", "Soja"],
  },
  {
    name: "Yogures",
    photo: "/assets/photoFood/Yogures.jpg",
    composition: "Leche,",
    allergens: ["Lacteos"],
  },
];

const findAllergens = async (foodItem) => {
  foodItem.allergens = await Promise.all(
    foodItem.allergens.map(async (allergenName) => {
      const allergen = await Allergens.findOne({ name: allergenName });
      if (allergen) {
        return allergen.id;
      } else {
        console.log("no existe ese alérgeno");
        return "no existe";
      }
    })
  );
};

connection
  .then(async () => {
    const allFood = await Food.find();

    if (allFood.length) {
      await Food.collection.drop();
    }
  })
  .catch((err) => console.log(`Error deleting data: ${err}`))
  .then(async () => {
    const foodDocuments = await Promise.all(
      foodArray.map(async (foodItem) => {
        await findAllergens(foodItem);
        return new Food(foodItem);
      })
    );
    await Food.insertMany(foodDocuments);
  })
  .catch((err) => console.log(`Error creating data: ${err}`))

  .finally(() => mongoose.disconnect());
