// eslint-disable-next-line require-jsdoc
function isAuthenticated(req, res, next) {
  console.log('Ver si el usuario está logado: ', req.user);
  if (req.isAuthenticated()) {
    return next();
  } else {
    console.log('Permiso denegado. Usuario no identificado.')
    return res.redirect('/');
  }
}

module.exports = { isAuthenticated };
