import { makeStyles } from '@material-ui/core/styles';
const useStyles = makeStyles({
    
    allergensButtom: {
        background: 'rgba(38,199,220,.8)',
        border: 0,
        borderRadius: 8,
        boxShadow: '0 3px 5px 2px rgba(255, 105, 135, .3)',
        color: 'white',
        height: 40,
        marginTop: '5%',
        width: 200,
        display: 'flex',
      },
      saveButtom: {
        border: 0,
        background: 'primary',
        borderRadius: 8,
        height: 40,
        marginTop: '15%',
        width: 120,
        display: 'flex',
      },
      profileButtom: {
         marginBottom: '10%',
         marginTop: '5%',
      }

});
export default  useStyles;