import React, { useEffect } from "react";
import { useSelector } from "react-redux";
import { authSelector } from "../../store/auth";
import { Link } from "react-router-dom";
import getUserApi from "../../api/getUser";
import routes from "../../config/routes";
// External Components
import Navbar from "../../components/Navbar";
// Material ui
import { Button } from "@material-ui/core";
import SearchIcon from "@material-ui/icons/Search";
import PersonIcon from "@material-ui/icons/Person";
import FastfoodIcon from "@material-ui/icons/Fastfood";
import FavoriteBorderIcon from "@material-ui/icons/FavoriteBorder";
import LocalHospitalIcon from "@material-ui/icons/LocalHospital";
// Styles
import "../../styles/HomePage/Home.scss";
import useStyles from "./HomeStyles";

const getUser = async (userId) => {
  try {
    await getUserApi(userId);
    // console.log(data);
  } catch (error) {
    console.log("Error al traer los datos del usuario.");
  }
};

const Home = () => {
  const { userId, userName } = useSelector(authSelector);
  useEffect(() => {
    getUser(userId);
    return () => {};
  }, [userId]);

  const classes = useStyles();
  return (
    <div className="Home">
      <header className="navbar">
        <span>
          <Navbar></Navbar>
        </span>
        <span>¡Hola {userName}!</span>
      </header>
      <div className="HomeLogo">
        <img
          src={process.env.PUBLIC_URL + "/assets/home/applergiclogo.png"}
          alt="logo"
        />
      </div>
      <div className="HomeButtons">
        <Button
          component={Link}
          to={routes.allFood}
          className={classes.homeButton1}
          variant="contained"
          color="primary"
        >
          <span>
            <SearchIcon></SearchIcon>
          </span>
          <span>Alimentos</span>
        </Button>
        <Link to={routes.allFood}>
          <p className="p">Busca y selecciona un alimento</p>
        </Link>

        <Button
          component={Link}
          to={routes.allAllergens}
          className={classes.homeButton2}
          variant="contained"
          color="primary"
        >
          <span>
            <SearchIcon></SearchIcon>
          </span>
          <span>Alergias</span>
        </Button>
        <Link to={routes.allAllergens}>
          <p className="p">Busca y selecciona tus alergias</p>
        </Link>

        <Button
          component={Link}
          to={routes.sos}
          className={classes.homeButton3}
          variant="contained"
          color="primary"
        >
          <span>
            <LocalHospitalIcon></LocalHospitalIcon>
          </span>
          <span>S.O.S.</span>
        </Button>
        <Link to={routes.sos}>
          <p className="p">¿Necesitas ayuda? Contactamos con emergencias.</p>
        </Link>
      </div>
      <ul className="Footer">
        <li className="Footer__li">
          <Link to={routes.user}>
            <PersonIcon className={classes.homeIcon}></PersonIcon>
          </Link>
        </li>
        <li className="Footer__li">
          <Link to={routes.userAllergens}>
            <FavoriteBorderIcon className={classes.icons}></FavoriteBorderIcon>
          </Link>
        </li>
        <li className="Footer__li">
          <Link to={routes.favFood}>
            <FastfoodIcon className={classes.icons}></FastfoodIcon>
          </Link>
        </li>
      </ul>
    </div>
  );
};

export default Home;
