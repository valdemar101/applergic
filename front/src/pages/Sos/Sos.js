import React, { useEffect, useState } from "react";
import { Link } from "react-router-dom";
import { useSelector } from "react-redux";
import { authSelector } from "../../store/auth";
import getUserApi from "../../api/getUser";
import routes from "../../config/routes";

// External Components
import Navbar from "../../components/Navbar";
import Grid from "@material-ui/core/Grid";
// Material ui
import Paper from "@material-ui/core/Paper";
import Typography from "@material-ui/core/Typography";
import { Container } from "@material-ui/core";
import Button from "@material-ui/core/Button";
// Styles
import "../../styles/SosPage/stylesSos.scss";
import useStyles from "./style";


import ".";

const Sos = () => {
  const classes = useStyles();
  const { userId, userName } = useSelector(authSelector);
  const [data, setData] = useState();

  const getDataUser = async (userId) => {
    const response = await getUserApi(userId);
    setData(response);
  };

  useEffect(() => {
    getDataUser(userId);
  }, [userId]);

  return data ? (
    <div className="Sos">
      <header className="navbar">
        <span>
          <Navbar></Navbar>
        </span>
        <span>¡Hola {userName}!</span>
      </header>
      <Container className="user-data">
        <Paper square className={classes.paper}>
          {data.photo && <img className="user-data__photo" src={data.photo} alt={data.name}></img>}
          <Typography className="user-data__item" variant="h5">
            {data.name}
          </Typography>
          <Typography className="user-data__item">
            Email: {data.email}
          </Typography>
          {data.phone && (
            <Typography className="user-data__item">
              {" "}
              Teléfono: {data.phone}
            </Typography>
          )}
        </Paper>

        <Paper className={classes.paper}>
          <Grid item xs>
            {data.contact ? (
              <div className="user-data__item">
                CONTACTO DE EMERGENCIA
                <h4>Nombre: {data.contact.name}</h4>
                <h4>Email: {data.contact.email}</h4>
                <h4>Teléfono: {data.contact.phone}</h4>
                <div>
                  {data.contact.phone ? (
                    <a href={`tel:+34${data.contact.phone}`}>
                      <Button
                        type="submit"
                        fullWidth
                        variant="contained"
                        color="primary"
                      >
                        LLAMADA DE EMERGENCIA
                      </Button>
                    </a>
                  ) : (
                    <h4>
                      No tenemos número registrado{" "}
                      <Button
                        component={Link}
                        to={routes.contactRegister}
                        type="submit"
                        fullWidth
                        variant="contained"
                        color="primary"
                      >
                        Registrar número
                      </Button>
                    </h4>
                  )}
                </div>
              </div>
            ) : (
              <div>
                <Link to={routes.contactRegister}>
                  <h4>No hay datos de tu persona de confianza</h4>
                </Link>
                <Button
                  component={Link}
                  to={routes.contactRegister}
                  type="submit"
                  fullWidth
                  variant="contained"
                  color="primary"
                >
                  Registrar Contacto
                </Button>
              </div>
            )}
          </Grid>
        </Paper>
        <Paper className={classes.paper}>
          <Grid item xs>
            {data.insuranceCompany ? (
              <div className="user-data__item">
                COMPAÑÍA DE SEGUROS
                <h4>Compañía: {data.insuranceCompany.name}</h4>
                <h4>
                  Número de póliza: {data.insuranceCompany.insuranceNumber}
                </h4>
                <h4>Teléfono: {data.insuranceCompany.phone}</h4>
                <div>
                  {data.insuranceCompany.phone ? (
                    <a href={`tel:+34${data.insuranceCompany.phone}`}>
                      <Button
                        type="submit"
                        fullWidth
                        variant="contained"
                        color="primary"
                      >
                        LLAMADA DE EMERGENCIA
                      </Button>
                    </a>
                  ) : (
                    <h4>
                      No tenemos número registrado{" "}
                      <Button
                        component={Link}
                        to={routes.insuranceCompanyRegister}
                        type="submit"
                        fullWidth
                        variant="contained"
                        color="primary"
                      >
                        Registrar número
                      </Button>
                    </h4>
                  )}
                </div>
              </div>
            ) : (
              <div>
                <Link to={routes.insuranceCompanyRegister}>
                  <h4>No hay datos de tu compañía de Seguros</h4>
                </Link>
                <Button
                  component={Link}
                  to={routes.insuranceCompanyRegister}
                  type="submit"
                  fullWidth
                  variant="contained"
                  color="primary"
                >
                  Registrar Compañía de Seguros
                </Button>
              </div>
            )}
          </Grid>
        </Paper>
      </Container>
    </div>
  ) : (
    <p>...loading data</p>
  );
};
export default Sos;
