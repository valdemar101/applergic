import React, { useEffect, useState } from "react";
import { Link, useHistory } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import { authSelector } from "../../store/auth";
import getUserApi from "../../api/getUser";
import deleteUserApi from "../../api/deleteUser";
import { resetAuthUser } from "../../store/auth";
import routes from "../../config/routes";
// External components
import Navbar from "../../components/Navbar";
// Material ui
import Grid from "@material-ui/core/Grid";
import Paper from "@material-ui/core/Paper";
import { Alert } from "@material-ui/lab";
import Avatar from "@material-ui/core/Avatar";
import Typography from "@material-ui/core/Typography";
import { Container } from "@material-ui/core";
import EditOutlinedIcon from "@material-ui/icons/EditOutlined";
import Button from "@material-ui/core/Button";
// Styles
import "../../styles/UserPage/stylesUser.scss";
import useStyles from "./style";




import "./";

const User = () => {
  const classes = useStyles();
  const { userId, userName } = useSelector(authSelector);
  const [data, setData] = useState({});
  const [deleteUserBoolean, setDeleteUser] = useState(false);
  const history = useHistory();
  const dispatch = useDispatch();

  useEffect(() => {
    getDataUser();
    return () => {};
  }, []);

  const getDataUser = async () => {
    try {
      const response = await getUserApi(userId);
      setData(response);
    } catch (error) {
      console.log("Error al traer usuario.");
    }
  };

  const deleteUserStep1 = () => {
    setDeleteUser(true);
  };

  const deleteUser = async (userId) => {
    try {
      await deleteUserApi(userId);
      sessionStorage.removeItem("token_applergic");
      sessionStorage.removeItem("name_user_applergic");
      dispatch(resetAuthUser());
      history.push(routes.login);
    } catch (error) {
      console.log("Error al eliminar el usuario.");
    }
  };

  return data ? (
    <div className="FoodContainer">
      <header className="AllergensNavbar">
        <span>
          <Navbar></Navbar>
        </span>
        <span>¡Hola {userName}!</span>
      </header>
      <Container className="user-data">
        <Paper square className={classes.paper}>
          {data.photo ? (
            <div>
              <img className="user-data__photo" src={data.photo} alt={data.name}></img>
            </div>
          ) : (
            <Avatar />
          )}
          <Typography className="user-data__item" variant="h5">
            {data.name}
          </Typography>
          <Typography className="user-data__item">
            Email: {data.email}
          </Typography>
          {data.phone && (
            <Typography className="user-data__item">
              {" "}
              Teléfono: {data.phone}
            </Typography>
          )}
        </Paper>

        <Paper className={classes.paper}>
          <Grid item xs>
            {data.contact ? (
              <div className="user-data__item">
                CONTACTO DE EMERGENCIA:
                {data.contact.name!==undefined && <h4>Nombre: {data.contact.name}</h4>}
                {data.contact.email!==undefined && <h4>Email: {data.contact.email}</h4>}
                {data.contact.phone!==undefined && <h4>Teléfono: {data.contact.phone}</h4>}
              </div>
            ) : (
              <p className="user-data__item">
                No hay datos de tu contacto en caso de emergencia.
              </p>
            )}
            <Link to={routes.contactRegister}>
              <EditOutlinedIcon className={classes.editIcon}>
                Editar
              </EditOutlinedIcon>
            </Link>
          </Grid>
        </Paper>
        <Paper className={classes.paper}>
          <Grid item xs>
            {data.insuranceCompany ? (
              <div className="user-data__item">
                COMPAÑÍA DE SEGUROS:
                {data.insuranceCompany.name!==undefined && <h4>Nombre: {data.insuranceCompany.name}</h4>}
                {data.insuranceCompany.phone!==undefined && <h4>Teléfono: {data.insuranceCompany.phone}</h4>}
                {data.insuranceCompany.insuranceNumber!==undefined && <h4>Póliza: {data.insuranceCompany.insuranceNumber}</h4>}
              </div>
            ) : (
              <p className="user-data__item">
                No hay datos de tu compañía de seguros
              </p>
            )}
            <Link to={routes.insuranceCompanyRegister}>
              <EditOutlinedIcon className={classes.editIcon}>
                Editar
              </EditOutlinedIcon>
            </Link>
          </Grid>
        </Paper>
        <Paper className={classes.paper}>
            <h3 className="user-data__item">Alergias de {userName}.</h3>
            {data.allergies?.map((allergies) => {
              return (
                <div className="user-data__item" key={allergies._id}>
                  <h4>{allergies.name}</h4>
                  <img
                    className="user-data__allergie-photo"
                    src={allergies.photo}
                    alt={allergies.name}
                  ></img>
                </div>
              );
            })}

            <Button
              component={Link}
              to={routes.allAllergens}
              type="submit"
              fullWidth
              variant="contained"
              color="primary"
            >
              AÑADIR ALÉRGENO
            </Button>
        </Paper>
        <Paper className={classes.paper}>
            <h3 className="user-data__item">
              Lista de alimentos de {userName}.
            </h3>
            {data.favFood?.map((favFood) => {
              return (
                <div key={favFood._id} className="user-data__item">
                  <h4>{favFood.name}</h4>
                  <img
                    className="user-data__allergie-photo"
                    src={favFood.photo}
                    alt={favFood.name}
                  ></img>
                </div>
              );
            })}

            <Button
              component={Link}
              to={routes.allFood}
              type="sub mit"
              fullWidth
              variant="contained"
              color="primary"
            >
              AÑADIR ALIMENTO
            </Button>
        </Paper>
        {deleteUserBoolean && (
          <Paper className={classes.paper}>
              <Alert
                onClick={() => {
                  deleteUser(userId);
                }}
                variant="filled"
                severity="warning"
              >
                Va a darse de baja, ¿Está seguro {userName}?
              </Alert>
          </Paper>
        )}
        <Paper className={classes.paper}>
            <Button
              onClick={() => {
                deleteUserStep1();
              }}
              type="sub mit"
              fullWidth
              variant="contained"
              color="primary"
            >
              ELIMINAR CUENTA
            </Button>
        </Paper>
      </Container>
    </div>
  ) : (
    <p>...Loading data</p>
  );
};
export default User;
