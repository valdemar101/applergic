import { createSlice } from '@reduxjs/toolkit';

const initialState = {
  foodAllergens: [],
  error: undefined,
};
const {
  actions: {
    setFoodAllergens,
    setError,
    resetFoodAllergens,
  },
  reducer,
  name: foodAllergensName
} = createSlice({
  name: 'foodAllergens',
  initialState,
  reducers: {
    setFoodAllergens: (state, {payload}) => {
      return {...state, foodAllergens: payload };
    },
    setError: (state, {payload}) => {
      return {...state, error: payload };
    },
    resetFoodAllergens: () => {
      return initialState;
    },
  }
});

const foodAllergensSelector = (state) => state[foodAllergensName];

export {
  foodAllergensName,
  foodAllergensSelector,
  setFoodAllergens,
  resetFoodAllergens,
  setError,
}

export default reducer;