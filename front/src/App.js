import React from "react";
import {
  BrowserRouter as Router,
  Switch,
  Redirect,
} from "react-router-dom";
import { Provider } from "react-redux";
import store from "./store";
import routes from "./config/routes";
import Login from "./components/Login";
import User from "./pages/User";
import Home from './pages/Home';
import Sos from './pages/Sos';
import Splash from "./components/Splash";
import Onboarding from "./components/Onboarding";
// import RegisterButton from "./components/RegisterButton";
import Register from "./components/Register";
import OkRegister from "./components/OkRegister";
import UserContactRegister from "./components/UserContactRegister";
import InsuranceCompanyRegister from "./components/InsuranceCompanyRegister";
import UserAllergens from "./components/UserAllergens";
import FavFood from "./pages/FavFood";
import AllFood from "./components/AllFood"
import Evaluation from './components/EvaluationApp';
import NoUserRoute from './components/NoUserRoute';
import SecureRoute from './components/SecureRoute';
import UserAllergensPage from "./pages/UserAllergensPage/UserAllergensPage";

import theme from "./themeprovider";
import "./App.scss";

import {
  ThemeProvider,
} from "@material-ui/core/styles";



function App() {
  return (
    <ThemeProvider theme={theme}>
      <Provider store={store}>
        <div className="App">
          <Router>
            <Switch>
              <NoUserRoute exact path={routes.splash}>
                <Splash />
              </NoUserRoute>
              <NoUserRoute exact path={routes.login}>
                <Login />
              </NoUserRoute>
              <NoUserRoute exact path={routes.onboarding}>
                <Onboarding />
              </NoUserRoute>
              <SecureRoute exact path={routes.home}>
                <Home />
              </SecureRoute>
              <SecureRoute exact path={routes.user}>
                <User />
              </SecureRoute>
                <SecureRoute exact path={routes.sos}>
                <Sos />
              </SecureRoute>
              <NoUserRoute exact path={routes.userRegister}>
                <Register />
              </NoUserRoute>
              <SecureRoute exact path={routes.contactRegister}>
                <UserContactRegister />
              </SecureRoute>
              <SecureRoute exact path={routes.insuranceCompanyRegister}>
                <InsuranceCompanyRegister />
              </SecureRoute>
              <SecureRoute exact path={routes.okregister}>
                <OkRegister />
              </SecureRoute>
              <SecureRoute exact path={routes.evaluation}>
                <Evaluation />
              </SecureRoute>
              <SecureRoute exact path={routes.allAllergens}>
                <UserAllergens />
              </SecureRoute>
              <SecureRoute exact path={routes.userAllergens}>
                <UserAllergensPage />
              </SecureRoute>
              <SecureRoute exact path={routes.favFood}>
                <FavFood />
              </SecureRoute>
              <SecureRoute exact path={routes.allFood}>
                <AllFood />
              </SecureRoute>
              <Redirect to={routes.home} />
            </Switch>
          </Router>
        </div>
      </Provider>
    </ThemeProvider>
  );
}

export default App;
