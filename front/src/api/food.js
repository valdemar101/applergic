import apiClient from "./api";
const BACK = process.env.BACK || 4000;
const foodUrl = `http://localhost:${BACK}/food/`;

const foodApi = async () => {
  const response = await apiClient(`${foodUrl}`, {
    headers: {
      Accept: "application/json",
      "Access-Control-Allow-Origin": "*",
    },
    credentials: "include",
  }).then(
    (response) => response
  );
  return response;
};

export default foodApi;
