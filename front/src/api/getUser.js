import apiClient from "./api";

const BACK = process.env.BACK || 4000;
const userUrl = `http://localhost:${BACK}/users/user`;

const getUserApi = async (id) => {
  const response = await apiClient(`${userUrl}/${id}`, {
    headers: {
      Accept: "application/json",
      "Access-Control-Allow-Origin": "*",
    },
    credentials: "include",
  }).then((response) => response);
  return response;
};

export default getUserApi;
