import apiClient from "./api";

const BACK = process.env.BACK || 4000;
const deleteAllergenUserUrl = `http://localhost:${BACK}/users/user/allergen/`;

const deleteAllergenUserApi = async (id) => {
  const url = deleteAllergenUserUrl + id;
  const response = await apiClient(url, {
    method: "GET",
    headers: {
    Accept: "application/json",
    "Content-Type": "application/json",
    "Access-Control-Allow-Origin": "*",
  },
  credentials: "include",
  }).then((response) => response);
  return response;
};

export default deleteAllergenUserApi;