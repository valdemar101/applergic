import apiClient from "./api";

const BACK = process.env.BACK || 4000;
const deleteFavFoodrUrl = `http://localhost:${BACK}/users/user/favFood/`;

const deleteFavFoodUserApi = async (id) => {
  const url = deleteFavFoodrUrl + id;
  const response = await apiClient(url, {
    method: "GET",
    headers: {
    Accept: "application/json",
    "Content-Type": "application/json",
    "Access-Control-Allow-Origin": "*",
  },
  credentials: "include",
  }).then((response) => response);
  return response;
};

export default deleteFavFoodUserApi;