import apiClient from "./api";
const BACK = process.env.BACK || 4000;
const loginUrl = `http://localhost:${BACK}/users/login`;

const loginUser = async (email, password) => {
  const userData = { email, password };
  const response = await apiClient(loginUrl, {
    method: "POST",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
      "Access-Control-Allow-Origin": "*",
    },
    credentials: "include",
    body: JSON.stringify(userData),
  });
  return response;
};

export default loginUser;
