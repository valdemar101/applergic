import apiClient from "./api";

const BACK = process.env.BACK || 4000;
const foodDetailNameUrl = `http://localhost:${BACK}/food/name/`;

const getFoodDetailNameApi = async (name) => {
  const url=foodDetailNameUrl + name;
  const response = await apiClient(url, {
    headers: {
      Accept: "application/json",
      "Access-Control-Allow-Origin": "*",
    },
    credentials: "include",
  }).then((response) => response);
  return response;
};

export default getFoodDetailNameApi;
