import apiClient from "./api";

const BACK = process.env.BACK || 4000;
const deleteUserUrl = `http://localhost:${BACK}/users/user/`;

const deleteUserApi = async (id) => {
  const url = deleteUserUrl + id;
  const response = await apiClient(url, {
    method: "DELETE",
    headers: {
    Accept: "application/json",
    "Content-Type": "application/json",
    "Access-Control-Allow-Origin": "*",
  },
  credentials: "include",
  }).then((response) => response);
  return response;
};

export default deleteUserApi;