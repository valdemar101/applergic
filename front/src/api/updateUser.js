import apiClient from "./api";

const BACK = process.env.BACK || 4000;
const registerContactUrl = `http://localhost:${BACK}/users/user/`;

const updateUser = async (userFormData, userId) => {
  const userData = userFormData;
  // console.log(userFormData);
  const url = registerContactUrl + userId;
  const response = apiClient(url, {
    method: "PATCH",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
      "Access-Control-Allow-Origin": "*",
    },
    body: JSON.stringify(userData),
    credentials: "include",
  });
  return response;
};

export default updateUser;