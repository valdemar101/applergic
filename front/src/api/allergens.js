import apiClient from "./api";
const BACK = process.env.BACK || 4000;
const allergensUrl = `http://localhost:${BACK}/allergens/`;

const allergensApi = async () => {
  const response = await apiClient(`${allergensUrl}`, {
    headers: {
      Accept: "application/json",
      "Access-Control-Allow-Origin": "*",
    },
    credentials: "include",
  }).then(
    (response) => response
  );
  return response;
};

export default allergensApi;
