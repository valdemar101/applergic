import apiClient from "./api";

const BACK = process.env.BACK || 4000;
const registerUrl = `http://localhost:${BACK}/users/register`;

const registerUser = async (userFormData) => {
  const userData = userFormData;
  const response = apiClient(registerUrl, {
    method: "POST",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
      "Access-Control-Allow-Origin": "*",
    },
    credentials: "include",
    body: JSON.stringify(userData),
  });
  return response;
};

export default registerUser;
