import apiClient from "./api";

const BACK = process.env.BACK || 4000;
const foodDetailUrl = `http://localhost:${BACK}/food/`;

const getFoodDetailApi = async (id) => {
  const url=foodDetailUrl + id;
  const response = await apiClient(url, {
    headers: {
      Accept: "application/json",
      "Access-Control-Allow-Origin": "*",
    },
    credentials: "include",
  }).then((response) => response);
  return response;
};

export default getFoodDetailApi;
