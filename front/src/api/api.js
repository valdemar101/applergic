const headers = {
  Accept: "application/json",
  "Content-Type": "application/json",
  "Access-Control-Allow-Origin": "*",
};

const apiClient = (url, config = { headers }) => {
  return fetch(url, config).then((response) => response.json());
};

// const apiClient = (url, config = { headers }) => {
//   return axios(url, config).then((response) => response);
// };


export default apiClient;
