import React, { useState, useEffect } from "react";
import { useDispatch } from "react-redux";
import routes from "../../config/routes";
import { useHistory, Link } from "react-router-dom";
import registerUser from "../../api/registerUser";
import logoutUserApi from "../../api/logoutUser";
import { setAuthUser } from "../../store/auth";
import { resetAuthUser } from "../../store/auth";

// External Components
import Copyright from "../Copyright";
// Material ui
// import Avatar from "@material-ui/core/Avatar";
import Button from "@material-ui/core/Button";
import { TextValidator, ValidatorForm } from "react-material-ui-form-validator";

// Styles
import "../../styles/Register/Register.scss";
import useStyles from "../../styles/Register/stylesRegister.js";

const Register = () => {
  const classes = useStyles();
  const history = useHistory();
  const dispatch = useDispatch();

  const emptyUser = {
    name: "",
    email: "",
    phone: "",
    password: "",
    photo: "",
  };

  const [userFormData, setUserFormData] = useState(emptyUser);
  const uploadedImage = React.useRef(null);
  const imageUploader = React.useRef(null);

  //limipamos por si acaso no se hiciera previamente un logout
  useEffect(() => {
    logout();
    return () => {};
  }, []);

  const logout = async () => {
    sessionStorage.removeItem("token_applergic");
    sessionStorage.removeItem("name_user_applergic");
    dispatch(resetAuthUser());
    await logoutUserApi();
  };

  const doRegister = async (userFormData) => {
    try {
      // console.log("Datos a registrar: ", userFormData);
      const data = await registerUser(userFormData);
      dispatch(setAuthUser({ id: data._id, name: data.name }));
      sessionStorage.setItem("token_applergic", data._id);
      sessionStorage.setItem("name_user_applergic", data.name);
      history.push(routes.contactRegister);
    } catch (error) {
      alert("Error al registrar.");
    }
  };

  const handleFormSubmit = async (event) => {
    event.preventDefault();
    doRegister(userFormData);
  };

  const handleChangeInput = (event) => {
    setUserFormData((prevValue) => ({
      ...prevValue,
      [event.target.name]: event.target.value,
    }));
  };

  const handleImageUpload = async (e) => {
    const [file] = e.target.files;
    if (file) {
      const reader = new FileReader();
      const { current } = uploadedImage;
      current.file = file;
      reader.onload = (e) => {
        current.src = e.target.result;
      };
      reader.readAsDataURL(file);
      // console.log(file);
      await uploadImage(e);
    }
  };

  const uploadImage = async (e) => {
    const files = e.target.files;
    const data = new FormData();
    data.append("file", files[0]);
    data.append("upload_preset", "ml_default");
    const res = await fetch(
      "https://api.cloudinary.com/v1_1/dj0zfm4sd/image/upload",
      { method: "POST", body: data }
    );
    const file = await res.json();
    if (file.secure_url !== undefined) {
      setUserFormData((prevValue) => ({
        ...prevValue,
        [e.target.name]: file.secure_url,
      }));
    }
  };

  return (
    <div className="Register">
      <div className="LogoRegister">
        <img
          src={process.env.PUBLIC_URL + "/assets/onboarding/logo.png"}
          alt="Applergic logo"
        />
      </div>
      <div className="TitleRegister">
        <ValidatorForm
          className="Form"
          onSubmit={handleFormSubmit}
          noValidate
          encType="multipart/form-data"
        >
          <div>
            <h1>Dinos quién eres.</h1>
            <span>
              {/* <Avatar> */}
                {/* este era el componente photo */}
                {imageUploader &&  (
                  <div  onClick={() => imageUploader.current.click()}>
                    <img
                      className="logo-photo"
                      ref={uploadedImage}
                      alt=""
                    />
                    <input
                      type="file"
                      accept="image/*"
                      name="photo"
                      id="photo"
                      onChange={handleImageUpload}
                      ref={imageUploader}
                      style={{
                        display: "none",
                      }}
                    />
                  </div>
                )}

                {/* hasta aqui llega componente photo */}
              {/* </Avatar> */}
            </span>
            <h3>Elige tu foto</h3>
          </div>
          <div>
            <TextValidator
              autoComplete="name"
              required
              name="name"
              variant="standard"
              validators={["required"]}
              errorMessages={["Campo requerido"]}
              fullWidth
              id="name"
              label="Name"
              autoFocus
              value={userFormData.name}
              onChange={handleChangeInput}
            />
            <TextValidator
              type="email"
              variant="standard"
              required
              fullWidth
              id="email"
              label="Email"
              name="email"
              validators={["required", "isEmail"]}
              errorMessages={[
                "Campo requerido",
                "El email introducido no es válido",
              ]}
              value={userFormData.email}
              onChange={handleChangeInput}
            />

            <TextValidator
              variant="standard"
              type="tel"
              inputProps={{ maxLength: 9 }}
              fullWidth
              id="phone"
              label="Telefono"
              name="phone"
              autoComplete=""
              value={userFormData.phone}
              onChange={handleChangeInput}
            />
            <TextValidator
              variant="standard"
              required
              validators={["required"]}
              errorMessages={["Campo requerido"]}
              fullWidth
              name="password"
              label="Password"
              type="password"
              id="password"
              autoComplete="current-password"
              value={userFormData.password}
              onChange={handleChangeInput}
            />
          </div>
          <Button
            type="submit"
            fullWidth
            variant="contained"
            className={classes.saveProfile}
          >
            Guardar perfil
          </Button>
          <div>
            <p>
              ¿Ya tienes cuenta?{" "}
              <Link className="Link" to={routes.login} variant="body2">
                Ir a Login
              </Link>
            </p>
          </div>
        </ValidatorForm>
      </div>
      <div className="CopyRight">
        <Copyright />
      </div>
    </div>
  );
};

export default Register;
