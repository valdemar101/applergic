import React from "react";
import { Avatar } from "@material-ui/core";

const  Photo = () => {
  const uploadedImage = React.useRef(null);
  const imageUploader = React.useRef(null);

  const handleImageUpload = e => {
    const [file] = e.target.files;
    if (file) {
      const reader = new FileReader();
      const {current} = uploadedImage;
      current.file = file;
      reader.onload = (e) => {
          current.src = e.target.result;
      }
      reader.readAsDataURL(file);
    }
  };

  return (
    <div
    style={{
      display: "flex",
      flexDirection: "column",
      alignItems: "center",
      justifyContent: "center"
    }}
  >
    <input
      type="file"
      accept="image/*"
      onChange={handleImageUpload}
      ref={imageUploader}
      style={{
        display: "none"
      }}
    />
    <div
    
      onClick={() => imageUploader.current.click()}>
        <Avatar><img
          ref={uploadedImage}
          style={{
            width: "40px",
            height: "40px",
            position: "absolute"
          }}
          alt={Photo}
        />
       </Avatar> 
      </div>
    </div>
  );
}

export default Photo;
