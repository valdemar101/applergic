import React from "react";
import routes from "../../config/routes";
import Copyright from "../Copyright/Copyright";
import { Link } from "react-router-dom";
import { useSelector } from "react-redux";
import { authSelector } from "../../store/auth";
// Material ui
import Button from "@material-ui/core/Button";
import CssBaseline from "@material-ui/core/CssBaseline";
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import Container from "@material-ui/core/Container";
import { CardMedia } from "@material-ui/core";
import { CardActionArea } from "@material-ui/core";
// Styles
import useStyles from "../../styles/OkRegister/stylesOkRegister";
import "../../styles/OkRegister/OkRegisterStyle.scss";

const OkRegister = () => {
  const classes = useStyles();
  const { userName } = useSelector(authSelector);
  return (
    <div className="OkRegister">
    <div className="LogoOkRegister">
        <img
          src={process.env.PUBLIC_URL + "/assets/onboarding/logo.png"}
          alt="Applergic logo"
        />
      </div>
      <Container component="main" maxWidth="xs">
        <CssBaseline />
        <Grid className={classes.image} />
        <Grid container direction="row" justify="center" alignItems="center">
          <CardActionArea>
            <CardMedia
              component="img"
              alt="Applergic"
              height="100%"
              image="/assets/register/ok.png"
              title="Ok"
            />
          </CardActionArea>
        </Grid>
        <Typography variant="h5" gutterBottom>
          Hemos terminado, {userName} ya puedes visitar tu perfil.
        </Typography>
        <Button
          component={Link}
          to={routes.user}
          type="submit"
          fullWidth
          variant="contained"
          color="primary"
        >
          Ver Perfil
        </Button>
      </Container>
      <div className="CopyRight">
        <Copyright />
      </div>
    </div>
  );
};

export default OkRegister;
