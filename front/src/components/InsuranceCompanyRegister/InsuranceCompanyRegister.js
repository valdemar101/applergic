import React, { useState } from "react";
import { useSelector } from "react-redux";
import routes from "../../config/routes";
import { useHistory, Link } from "react-router-dom";
import updateUser from "../../api/updateUser";
import { authSelector } from "../../store/auth";
import Copyright from "../Copyright";
// Material ui
import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";
import KeyboardArrowLeft from "@material-ui/icons/KeyboardArrowLeft";
// Styles
import useStyles from "../../styles/InsuranceCompanyRegister/stylesInsuranceCompanyRegister";


const InsuranceCompanyRegister = () => {
  const classes = useStyles();
  const history = useHistory();

  const emptyContact = {
    insuranceCompanyName: "",
    insuranceCompanyPhone: "",
    insuranceNumber: "",
  };

  const [userFormData, setUserFormData] = useState(emptyContact);
  const { userId } = useSelector(authSelector);

  const doUpdate = async (userFormData) => {
    try {
      if (
        userFormData.insuranceCompanyName === "" &&
        userFormData.insuranceCompanyPhone === "" &&
        userFormData.insuranceNumber === ""
      ) {
        // console.log("Introduce los datos o salta pantalla");
      } else {
        const data = await updateUser(userFormData, userId);
        history.push(routes.allAllergens, data);
      }
    } catch (error) {
      console.log("Error al actualizar.");
    }
  };

  const handleFormSubmit = async (event) => {
    event.preventDefault();
    doUpdate(userFormData);
  };

  const handleChangeInput = (event) => {
    setUserFormData((prevValue) => ({
      ...prevValue,
      [event.target.name]: event.target.value,
    }));
  };

  return (
    <div className="Register">
      <Button component={Link} to={routes.user} size="small">
        <KeyboardArrowLeft />
        Volver a Mi Perfil
      </Button>

      <div className="TitleContact">
        <h1>Vamos a añadir los datos de tu compañía de seguros.</h1>
        <h3>Nos pondremos en contacto con tu seguro en caso de emergencia.</h3>
      </div>

      <form className="Form" onSubmit={handleFormSubmit} noValidate>
        <TextField
          variant="standard"
          margin="normal"
          fullWidth
          id="insuranceCompanyName"
          label="Compañía de Seguros"
          name="insuranceCompanyName"
          autoComplete="insuranceCompanyName"
          autoFocus
          value={userFormData.insuranceCompanyName}
          onChange={handleChangeInput}
        />
        <TextField
          variant="standard"
          margin="normal"
          fullWidth
          id="insuranceCompanyPhone"
          label="Teléfono"
          name="insuranceCompanyPhone"
          type="tel"
          inputProps={{maxLength:9}}
          autoComplete="phone"
          value={userFormData.insuranceCompanyPhone}
          onChange={handleChangeInput}
        />
        <TextField
          variant="standard"
          margin="normal"
          fullWidth
          name="insuranceNumber"
          label="Nº Póliza"
          id="insuranceNumber"
          type="text"
          value={userFormData.insuranceNumber}
          onChange={handleChangeInput}
        />
        <Button
          type="submit"
          fullWidth
          variant="contained"
          color="primary"
          className={classes.submit}
        >
          Guardar Datos
        </Button>

        <Link className="ContactLink" to={routes.home}>
          Registraré mi compañía de seguros en otro momento
        </Link>
      </form>

      <div className="CopyRight">
        <Copyright />
      </div>
    </div>
  );
};

export default InsuranceCompanyRegister;
