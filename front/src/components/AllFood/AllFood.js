import React, { useState, useEffect } from "react";
import foodApi from "../../api/food";
import { useSelector } from "react-redux";
import { authSelector } from "../../store/auth";
// External Components
import FoodCard from "../FoodCard";
import Navbar from "../Navbar";
import getUserApi from "../../api/getUser";
import getFoodDetailNameApi from "../../api/foodDetailByName";
// Material ui
import { Container } from "@material-ui/core";
import { Paper } from "@material-ui/core";
import TextField from "@material-ui/core/TextField";
// Styles
import "../../styles/AllFood/stylesAllFood.scss";
import useStyles from "../../pages/Sos/style";

const AllFood = () => {
  const classes = useStyles();
  const { userName, userId } = useSelector(authSelector);
  const [food, setFood] = useState([]);
  const [foodDetail, setFoodDetail] = useState({});
  const [userAllergies, setUserAllergies] = useState([]);
  const [userFavFood, setUserFavFood] = useState([]);

  useEffect(() => {
    getFood();
    getUserData(userId);
    setFoodDetail()
    return () => {};
  }, [userId]);

  const getUserData = async (userId) => {
    try {
      const data = await getUserApi(userId);
      setUserAllergies(data.allergies);
      setUserFavFood(data.favFood);
    } catch (error) {
      console.log("Error al traer alergias del usuario.");
    }
  };

  const getFood = async () => {
    try {
      const foodData = await foodApi();
      setFood(foodData);
    } catch (error) {
      console.log("Error al traer alimentos.");
    }
  };

  const findFoodByName = (event) => {
    const nameToFind = event.target.value;
    getFoodDetailName(nameToFind);
  };

  const getFoodDetailName = async (nameToFind) => {
    try {
      const dataFoodDetail = await getFoodDetailNameApi(nameToFind);
      setFoodDetail(dataFoodDetail);
    } catch {
      //no existe el alimento, entonces limpiamos los detalles en el estado
      setFoodDetail();
    }
  };

  return (
    <div className="FoodContainer">
      <header className="AllergensNavbar">
        <span>
          <Navbar />
        </span>
        <span>¡Hola {userName}!</span>
      </header>
      {food.length > 0 && (
        <div>
          <div className="container-search-input">
            <TextField
              onChange={findFoodByName}
              type="text"
              placeholder="Buscar por nombre"
            ></TextField>
          </div>
          {foodDetail ? (
            <div>
              <h3>Detalles de {foodDetail.name}</h3>
              <Paper square className={classes.paper} key={food._id}>
                <FoodCard
                  foodId={foodDetail._id}
                  userAllergies={userAllergies}
                  userFavFood={userFavFood}
                />
              </Paper>
            </div>
          ) : (
            <div>
              <h3>Lista de alimentos:</h3>
              <Container className="user-data">
                {food.map((food) => {
                  return (
                    <Paper square className={classes.paper} key={food._id}>
                      <FoodCard
                        foodId={food._id}
                        userAllergies={userAllergies}
                        userFavFood={userFavFood}
                      />
                    </Paper>
                  );
                })}
              </Container>
            </div>
          )}
        </div>
      )}
    </div>
  );
};

export default AllFood;
