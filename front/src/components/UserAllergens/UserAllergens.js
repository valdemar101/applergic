import React, { useState, useEffect } from "react";
import { useSelector } from "react-redux";
import routes from "../../config/routes";
import { useHistory, Link } from "react-router-dom";
import updateUser from "../../api/updateUser";
import allergensApi from "../../api/allergens";
import { authSelector } from "../../store/auth";
import getUserApi from "../../api/getUser";
//External Components
import Navbar from "../Navbar";
import Copyright from "../Copyright";
// Material ui
import ArrowDropUpIcon from "@material-ui/icons/ArrowDropUp";
import Button from "@material-ui/core/Button";
// Styles
import useStyles from "../../styles/UserAllergens/stylesUserAllergens.js";
import "../../styles/UserAllergens/stylesUserAllergens.scss";

const UserAllergenscopy = () => {
  const classes = useStyles();
  const history = useHistory();

  const emptyAllergens = [];

  const { userId, userName } = useSelector(authSelector);
  const [allergens, setAllergens] = useState(emptyAllergens);
  const [allergensCharAt, setAllergensCharAt] = useState(emptyAllergens);
  const [userAllergens, setUserAllergens] = useState(emptyAllergens);
  const [userAllergensName, setUserAllergensName] = useState(emptyAllergens);
  const [userEvaluation, setUserEvaluation] = useState(undefined);

  useEffect(() => {
    getAllergens();
    getUser(userId);
    return () => {};
  }, [userId]);

  const getUser = async (userId) => {
    const data = await getUserApi(userId);
    if (data.evaluation) {
      setUserEvaluation(data.evaluation);
    } else {
      setUserEvaluation("noEvaluation");
    }
  };

  const getAllergens = async () => {
    try {
      const allergensData = await allergensApi();
      setAllergens(allergensData);
      //creamos un array con las iniciales de los alergenos
      const allergenName = [];
      allergensData.map((allergen) => {
        const nameChart = allergen.name.charAt(0);
        return allergenName.push(nameChart);
      });
      // eliminamos iniciales repetidas en el array de los alergenos
      let allergenNameFinal = Array.from(new Set(allergenName));
      setAllergensCharAt(allergenNameFinal);
    } catch (error) {
      console.log("Error al traer alérgenos.");
    }
  };

  const doUpdate = async (userAllergens) => {
    try {
      await updateUser({ allergies: userAllergens }, userId);
      if (userEvaluation === "noEvaluation") {
        history.push(routes.okregister);
      } else {
        history.push(routes.user);
      }
    } catch (error) {
      console.log("Error al actualizar.");
    }
  };

  const handleFormSubmit = async (event) => {
    event.preventDefault();
    doUpdate(userAllergens);
  };

  const handleClickButton = (id, name) => {
    const existAllergen = userAllergens.filter((item) => item === id);
    if (existAllergen.length > 0) {
      // si ya existe el alérgeno, lo borramos de la lista
      const removeAllergen = userAllergens.filter((item) => item !== id);
      setUserAllergens(removeAllergen);
      const removeAllergenName = userAllergensName.filter(
        (item) => item !== name
      );
      setUserAllergensName(removeAllergenName);
    } else {
      setUserAllergens((prevValue) => [...prevValue, id]);
      setUserAllergensName((prevValue) => [...prevValue, name]);
    }
  };

  return (
    <div className="Allergens">
      <header className="AllergensNavbar">
        <span>
          <Navbar></Navbar>
        </span>
        <span>¡Hola {userName}!</span>
      </header>
      <div className="TitleAllergens">
        <h1>Ahora selecciona tus alergias e intolerancias.</h1>
        <h3>
          Los elementos marcados serán identificados en tus búsquedas como
          peligrosos para ti.
        </h3>
      </div>
      <div id="start" className="form">
        {/* para mostrar los alérgenos seleccionados por el usuario */}
        {userAllergensName.length > 0 && (
          <div>
            <h3>Alergias seleccionadas:</h3>
            <div className="user-allergens-container">
              {userAllergensName.map((allergen) => {
                return (
                  <div
                    className="user-allergens-container__item"
                    key={allergen}
                  >
                    <h4>{allergen}</h4>
                  </div>
                );
              })}
            </div>
            <form onSubmit={handleFormSubmit}>
              <Button
                type="submit"
                className={classes.profileButtom}
                variant="contained"
                color="primary"
              >
                Guardar alergias seleccioandas
              </Button>
            </form>
          </div>
        )}
        <p component="h1" variant="h5">
          Selecciona tus alergias:
        </p>
        <div className="initials-container">
          {allergensCharAt?.map((allergenName) => {
            return (
              <div className="initials-container__item" key={allergenName}>
                <button>
                  <a href={"#" + allergenName}>{allergenName}</a>
                </button>
              </div>
            );
          })}
        </div>
        <div>
          {allergensCharAt?.map((allergenName) => {
            return (
              <div key={allergenName}>
                <h4 id={allergenName} className="form__input">
                  {allergenName}
                </h4>
                <a href="#start">
                  <ArrowDropUpIcon />
                </a>
                {allergens?.map((allergen) => {
                  return (
                    <div key={allergen._id}>
                      {allergen.name.charAt(0) === allergenName && (
                        <Button
                          className={classes.allergensButtom}
                          variant="contained"
                          onClick={() => {
                            handleClickButton(allergen._id, allergen.name);
                          }}
                        >
                          {allergen.name}
                        </Button>
                      )}
                    </div>
                  );
                })}
              </div>
            );
          })}
        </div>
        <form onSubmit={handleFormSubmit}>
          <Button
            type="submit"
            className={classes.saveButtom}
            variant="contained"
            color="primary"
          >
            Guardar
          </Button>
        </form>
          <Button component={Link} to={routes.user}>Ir a Perfil</Button>
      </div>
      <div className="AllergensCopyRight">
        <Copyright />
      </div>
    </div>
  );
};

export default UserAllergenscopy;
