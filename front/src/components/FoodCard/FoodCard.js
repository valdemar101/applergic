import React, { useState, useEffect } from "react";
import getFoodDetailApi from "../../api/foodDetail";
import { useSelector } from "react-redux";
import { authSelector } from "../../store/auth";
import updateUser from "../../api/updateUser";
import getUserApi from "../../api/getUser";
// Material ui
import { Button } from "@material-ui/core";
import ReportProblemOutlinedIcon from '@material-ui/icons/ReportProblemOutlined';
import VerifiedUserOutlinedIcon from '@material-ui/icons/VerifiedUserOutlined';
import CloudDoneOutlinedIcon from '@material-ui/icons/CloudDoneOutlined';
// Styles
import "../../styles/FoodCard/stylesFoodCards.scss";



const FoodCard = (props) => {
  const { userId } = useSelector(authSelector);
  const [userFavFood, setUserFavFood] = useState();
  const [foodDetail, setFoodDetail] = useState([]);
  const [existFood, setExistFood] = useState(false);
  const [withAllergen, setWithAllergen] = useState(false);

  useEffect(() => {
    getFoodDetail();
    return () => {};
  }, [userFavFood]);

  useEffect(() => {
    compareAllergens();
    return () => {};
  }, [foodDetail]);

  const getUserData = async (userId) => {
    try {
      const data = await getUserApi(userId);
      setUserFavFood(data.favFood);
    } catch (error) {
      console.log("Error al traer alimentos del usuario.");
    }
  };

  const getFoodDetail = async () => {
    try {
      const foodDetailData = await getFoodDetailApi(props.foodId);
      setFoodDetail(foodDetailData);
    } catch (error) {
      console.log("Error al traer detalles del alimento.");
    }
  };

  const doUpdate = async (foodId) => {
    try {
      await updateUser({ favFood: foodId }, userId);
      setWithAllergen(false);
      //tengo q traer los alimentos para actulizar los datos
      getUserData(userId);
    } catch (error) {
      console.log("Error al actualizar.");
    }
  };
  const compareAllergens = async () => {
    const userAllergies = props.userAllergies;
    let favFood = [];
    if (userFavFood === undefined) {
      favFood = props.userFavFood;
    } else {
      favFood = userFavFood;
    }

    let filterBoleanExistAllergen = false;
    userAllergies?.map((allergie) => {
      if (
        foodDetail.allergens?.filter(
          (allergen) => allergen._id === allergie._id
        ).length > 0
      ) {
        // console.log("Contiene alérgenos del usuario");
        filterBoleanExistAllergen = true;
        return setWithAllergen(true);
      }
      return {};
    });
    if (filterBoleanExistAllergen === false) {
      if (favFood?.filter((item) => item._id === props.foodId).length > 0) {
        setExistFood(true);
        // console.log("Ya existe alimento en usuario");
      }
    }
  };

  const compareAllergensHandleClick = () => {
    const userAllergies = props.userAllergies;
    let favFood = [];
    if (userFavFood === undefined) {
      favFood = props.userFavFood;
    } else {
      favFood = userFavFood;
    }
    let filterBolean = false;
    userAllergies?.map((allergie) => {
      if (
        foodDetail.allergens?.filter(
          (allergen) => allergen._id === allergie._id
        ).length > 0
      ) {
        // console.log("Contiene alérgenos del usuario");
        filterBolean = true;
        setWithAllergen(true);
      }
      return {};
    });
    if (filterBolean === false) {
      if (favFood?.filter((item) => item._id === props.foodId).length === 0) {
        // console.log("No contiene alérgenos del usuario");
        doUpdate(props.foodId);
      } else {
        setExistFood(true);
        // console.log("Ya existe alimento en usuario");
      }
    }
  };

  return (
    <div>
      {withAllergen === true && (
        <div className="container-title-msg">
          <span>
            <ReportProblemOutlinedIcon fontSize="small" />
          </span>
          <h5 className="txt-alert">Contiene alérgenos</h5>
        </div>
      )}

      {existFood === true && (
        <div className="container-title-msg">
          <span>
            <CloudDoneOutlinedIcon fontSize="small" />
          </span>
          <h5 className="txt-green">Alimento en su lista de favoritos.</h5>
        </div>
      )}
      {existFood === false && withAllergen === false ? (
        <div className="container-title-msg">
          <span>
            <VerifiedUserOutlinedIcon fontSize="small" />
          </span>
          <h5 className="txt-green">Alimento seguro.</h5>
        </div>
      ): null}
      <h3>{foodDetail.name}</h3>
      <img
        className="photo-allergen-big"
        src={foodDetail.photo}
        alt={foodDetail.name}
      ></img>
      <h4>Composición:</h4>
      <p>{foodDetail.composition}</p>
      <h4>Alérgenos:</h4>
      {foodDetail.allergens?.map((allergen) => {
        return (
          <div key={allergen._id}>
            <p>{allergen.name}</p>
            <img
              className="photo-allergen"
              src={allergen.photo}
              alt={allergen.name}
            ></img>
          </div>
        );
      })}
      {existFood === false && withAllergen === false ? (
        <Button
          className="FoodCardButton"
          variant="contained"
          color="primary"
          onClick={compareAllergensHandleClick}
        >
          Añadir alimento a favoritos
        </Button>
      ) : null}
    </div>
  );
};

export default FoodCard;
