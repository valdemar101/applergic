import React, { useState, useEffect } from "react";
import { useDispatch } from "react-redux";
import routes from "../../config/routes";
import { useHistory, Link } from "react-router-dom";
import loginUser from "../../api/loginUser";
import logoutUserApi from "../../api/logoutUser";
import { setAuthUser } from "../../store/auth";
import { resetAuthUser } from "../../store/auth";

// External components
import Copyright from "../Copyright";
// Material ui
import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";
// Styles
import "../../styles/Login/Login.scss";
import useStyles from "../../styles/Login/stylesLogin";

const Login = () => {
  const classes = useStyles();
  const history = useHistory();
  const dispatch = useDispatch();

  const emptyUser = {
    email: "",
    password: "",
  };

  const [userFormData, setUserFormData] = useState(emptyUser);

  //limipamos por si acaso no se hiciera previamente un logout
  useEffect(() => {
    logout();
    return () => {};
  }, []);

  const logout = async () => {
    sessionStorage.removeItem("token_applergic");
    sessionStorage.removeItem("name_user_applergic");
    dispatch(resetAuthUser());
    await logoutUserApi();
  };

  const doLogin = async ({ email, password }) => {
    try {
      const data = await loginUser(email, password);
      dispatch(setAuthUser({ id: data._id, name: data.name }));
      sessionStorage.setItem("token_applergic", data._id);
      sessionStorage.setItem("name_user_applergic", data.name);
      history.push(routes.home);
    } catch (error) {
      alert("Error, usuario o contraseña incorrectos.");
      // console.log("Error, usuario o contraseña incorrectos.");
    }
  };

  const handleFormSubmit = async (event) => {
    event.preventDefault();
    doLogin(userFormData);
  };

  const handleChangeInput = (event) => {
    setUserFormData((prevValue) => ({
      ...prevValue,
      [event.target.name]: event.target.value,
    }));
  };

  return (
    <div className="Login">
    <div>
    <img
         src={process.env.PUBLIC_URL + "/assets/login/imagefood.png"}
         alt="Applergic food"
       />
    </div>
      <div className="TitleLogin">
        <h1>¡Bienvenido de nuevo!</h1>
        <h3>Porfavor, introduce tus datos para continuar</h3>
      </div>
      <div>
        <form className="Form" onSubmit={handleFormSubmit} noValidate>
          <TextField
            variant="standard"
            margin="normal"
            required
            fullWidth
            id="email"
            label="Email"
            name="email"
            autoComplete="email"
            autoFocus
            value={userFormData.email}
            onChange={handleChangeInput}
          />
          <TextField
            variant="standard"
            margin="normal"
            required
            fullWidth
            name="password"
            label="Password"
            type="password"
            id="password"
            value={userFormData.password}
            onChange={handleChangeInput}
            autoComplete="current-password"
          />
          <div>
          <p>Inicia tu sesión</p>
          <Button
            type="submit"
            variant="contained"
            className={classes.loginButtom}
          >
            Login
          </Button>
          </div>
        </form>
        <div>
          <p>¿Nuevo en Applergic?</p>
          <Button component={Link} to={routes.userRegister}
            className={classes.registerButtom} 
            variant="contained">Regístrate
          </Button>
        </div>
      </div>

      <div className="CopyRight">
        <Copyright />
      </div>
    </div>
  );
};

export default Login;