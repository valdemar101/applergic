import React from "react";
import { Link } from "react-router-dom";
import routes from "../../config/routes";

// Material ui
import { useTheme } from "@material-ui/core/styles";
import MobileStepper from "@material-ui/core/MobileStepper";
import Typography from "@material-ui/core/Typography";
import Button from "@material-ui/core/Button";
import KeyboardArrowLeft from "@material-ui/icons/KeyboardArrowLeft";
import KeyboardArrowRight from "@material-ui/icons/KeyboardArrowRight";
import SwipeableViews from "react-swipeable-views";
import { autoPlay } from "react-swipeable-views-utils";
import { slideImages } from "./slideImages";
// Styles
import useStyles from "../../styles/Onboarding/stylesOnboarding";
import "../../styles/Onboarding/Onboarding.scss";

const AutoPlaySwipeableViews = autoPlay(SwipeableViews);

const Onboarding = () => {
  const classes = useStyles();
  const theme = useTheme();
  const [activeStep, setActiveStep] = React.useState(0);
  const maxSteps = slideImages.length;

  const handleNext = () => {
    setActiveStep((prevActiveStep) => prevActiveStep + 1);
  };

  const handleBack = () => {
    setActiveStep((prevActiveStep) => prevActiveStep - 1);
  };

  const handleStepChange = (step) => {
    setActiveStep(step);
  };

  return (
    <div className="Onboarding">
    <div className="Logo">
      <img
         src={process.env.PUBLIC_URL + "/assets/onboarding/logo.png"}
         alt="Applergic logo"
       />
    </div>
     <div className="ImagesSlider">
     <AutoPlaySwipeableViews
        axis={theme.direction === "rtl" ? "x-reverse" : "x"}
        index={activeStep}
        onChangeIndex={handleStepChange}
        enableMouseEvents
      >
        {slideImages.map((step, index) => (
          <div key={step.label}>
            {Math.abs(activeStep - index) <= 2 ? (
              <img
                className={classes.img}
                src={step.imgPath}
                alt={step.label}
              />
            ) : null}
          </div>
        ))}
      </AutoPlaySwipeableViews>
     </div>
      <div className="TextSlider">
        <Typography>{slideImages[activeStep].label}</Typography>
      </div>
      <MobileStepper
        steps={maxSteps}
        position="static"
        variant="dots"
        activeStep={activeStep}
        nextButton={
          <div>
            {activeStep <= maxSteps - 2 ? (
              <Button size="small" onClick={handleNext}>
                SIGUIENTE
                {theme.direction === "rtl" ? (
                  <KeyboardArrowLeft />
                ) : (
                  <KeyboardArrowRight />
                )}
              </Button>
            ) : (
                <Button component={Link} to={routes.login} size="small">
                  LOGIN
                  {theme.direction === "rtl" ? (
                    <KeyboardArrowLeft />
                  ) : (
                    <KeyboardArrowRight />
                  )}
                </Button>
            )}
          </div>
        }
        backButton={
          <Button size="small" onClick={handleBack} disabled={activeStep === 0}>
            {" "}
            atras
            {theme.direction === "rtl" ? (
              <KeyboardArrowRight />
            ) : (
              <KeyboardArrowLeft />
            )}
          </Button>
        }
      />
    </div>
  );
};

export default Onboarding;
