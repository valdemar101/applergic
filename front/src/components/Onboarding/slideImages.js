export const slideImages = [
  {
    imgPath: "/assets/onboarding/scan2.png",
    label:
      "¡Bienvenido a Applergic! Escanea el código de barras de tu producto y Applergic te dirá si es apto para ti.",
  },
  {
    imgPath: "/assets/onboarding/rectangle.png",
    label: "Lleva tu Diario de compras y actividades.",
  },
  {
    imgPath: "/assets/onboarding/ambulancia.png",
    label:
      "En caso de emergencia nos pondremos en contacto con la persona que nos digas.",
  },
  {
    imgPath: "/assets/onboarding/traduccion.png",
    label:
      "Viaja a donde quieras. Tendrás a tu disposición un traductor off-line y tu informe de alergias e intolerancias traducido al idioma local.",
  },
];